# aws-recognition

amazon recognition, deep learning, face and image recognition service

Amazon Recognition makes it easy to add image analysis to your applications using proven, highly scalable, deep learning technology that requires no machine learning expertise to use.
With Amazon Recognition, you can identify objects, people, text, scenes, and activities in images, as well as detect any inappropriate content.

Amazon Recognition also provides highly accurate facial analysis and facial search 
capabilities that you can use to detect, analyze, and compare faces for a wide variety of user verification, people counting, and public safety use cases.


Set Up:
Create IAM User with Amazon Recognition and Amazon S3 policies attached.
Simply include into the configurations your AWS IAM User Access and Secret Access Key and your AWS S3 Bucket Name and you are all set!


**Benefits of Amazon Recognition:**
Deep Learning Technology
No Machine Learning expertise required
Provides highly accurate facial analysis
Provides highly accurate facial search capabilities
Identify thousands of objects and scenes
Identify potentially unsafe or inappropriate content
Read skewed and distorted text to capture various information
Quickly identify well known people in the image
Identify products, landmarks and brands
Detect up to 100 faces in an image
Compare up to 15 detected faces
Adjust probability of given prediction
Up to 15MB stored as Amazon S3 object size (Image File size)
Support PNG & JPEG image formats
Can detect up to 50 Words in an image
“Pay as you go” payment model
Easy to customize

Cost of Running Amazon Recognition – Image Recognition Service:
You can use any hosting platform as you prefer for the application itself
AWS Account (Free to Open – You will be on Free Tier for the 1st year)
Amazon S3 Storage Cost (For Data Storage and Data Traffic Out)
As part of the AWS Free Tier, you can get started with Amazon Recognition Image at no cost. The Free Tier lasts 12 months and allows you analyze 5,000 images per month and store 1,000 pieces of face metadata per month.

Amazon Recognition charges you each time you analyze an image using our APIs. Running multiple APIs against a single image counts as processing multiple images.

**Installation Instructions:**

AWS PHP SDK v3 is Required (Already comes with the App) – Setup Link
AWS IAM User Amazon Rekognition and Amazon S3 Access Policies attached – Setup Link
Amazon S3 Bucket with Public Access – Setup Link
Also Listed and Explained in the Documentation

*Credits:
Foggy by Nbartlomiej
jCanvas by Caleb Evans*
Berkine Design
